#!/bin/bash
#
## scp woodylee@emmasvn.cmes.com.tw:/home/woodylee/backup/backup-emmasvn_2015/20151225233001-GIT-Emma.7z ./
# sync -az --numeric-ids --stats --human-readable --delete  woodylee@emmasvn.cmes.com.tw:/home/woodylee/backup/emmasvn-config ./
#@ https://www.namhuy.net/2433/ssh-login-without-password.html
# rsync -az -e "ssh" --numeric-ids --stats --human-readable --delete /home/woodylee/backup/backup-emmasvn  emmauser@emma168.cmes.com.tw:/home/emmauser/emma168-rsync-backup
# echo $(ls -1t *.rar | head -1)
# 30 23 * * 5 /home/woodylee/backup/backup-emmasvn.sh

DIR168_EMMASVN_BACKUP=/home/emmauser/emma168-rsync-backup/backup-emmasvn
#DIR168_EMMASVN_BACKUP=/Users/chengerin/Dropbox/_work/_aalm/backup-emmasvn
cd ${DIR168_EMMASVN_BACKUP}

DIR168_GITTEMP=${DIR168_EMMASVN_BACKUP}/../gittemp

#EMMA_GIT=${DIR168_EMMASVN_BACKUP}/emma168-backup/emmasvn_git-backup
EMMA_GITFILE=$(ls -1t *GIT-*.7z | head -1) #GIT-Emma.7z
EMMA_READMINEFILE=$(ls -1t *www-redmine*.7z | head -1) #www-redmine.7z
EMMA_READMINE_PLUGINSFILE=$(ls -1t *redmine_plugins*.7z | head -1) #../redmine_plugins.7z
EMMA_SQLFILE=$(ls -1t *sqlsdump-redmine*.gz | head -1) #mysqldump.sql.gz
EMMA_REDMINE_TEMP=/opt/redmine/data

#USE_EMMAGIT=1
USE_EMMAREDMINE=1
USE_EMMAREDMINE_PLUGINS=1

if [[ -n ${USE_EMMAGIT} ]]; then

echo "###### Sync Emma Git #######"

#mkdir -p ${DIR168_GITTEMP}
#mv ${DIR168_EMMASVN_BACKUP}/emma168-backup/emmasvn_backup/${EMMA_GITFILE} ${EMMA_GIT}/
echo "###### 7z Emma Git ####### "${EMMA_GITFILE}
7z x -y ${EMMA_GITFILE} -o${DIR168_GITTEMP}
rsync -av --human-readable --delete --ignore-existing ${DIR168_GITTEMP}/ /srv/docker/gitlab/gitlab/repositories/Emma/
#rm -rf ${DIR168_GITTEMP}
#rm -rf ${EMMA_GIT}/*GIT-Emma.7z

docker exec -i aalm_gitlab_1 bash << EOF
chown -R git:git /home/git/data/repositories/
cd /home/git/gitlab
bundle exec rake gitlab:import:repos RAILS_ENV=production
EOF

fi

if [[ -n ${USE_EMMAREDMINE} ]]; then

echo "###### Sync Mysql Emmasvn redmine db #######"

#7z x -y ${EMMA_SQLFILE} -o/srv/docker/mysql/data/

#mkdir -p /opt/mysql/emmasvn-redmine-db-temp
rsync -av --stats --human-readable --ignore-existing ${EMMA_SQLFILE} /opt/mysql/data/
#rm -rf ${DIR168_EMMASVN_BACKUP}/emma168-backup/emmasvn_backup/emmasvn_redmine-backup/db/${EMMA_SQLFILE}

docker exec -i aalm_mysql_1 bash << EOF
cd /var/lib/mysql
gunzip < ${EMMA_SQLFILE} | mysql -uroot --default-character-set=utf8
#mysql -uroot --default-character-set=utf8 < ${EMMA_SQLFILE}
mysql -uroot  << EOF
use redmine;
show tables;
UPDATE users SET hashed_password='353e8061f2befecb6818ba0c034c632fb0bcae1b' WHERE login='admin';
UPDATE users SET salt='' WHERE login='admin';
EOF

EOF

#rm -rf  /opt/mysql/data/${EMMA_SQLFILE}
fi

if [[ -n ${USE_EMMAREDMINE_PLUGINS} ]]; then

echo "###### Sync Emma Redmine #######"
mkdir -p ${EMMA_REDMINE_TEMP}/tmp/redmine0/plugins
7z x -y ${EMMA_READMINEFILE} -o${EMMA_REDMINE_TEMP}/tmp/redmine0
#rsync -av --ignore-existing ${DIR168_EMMASVN_BACKUP}/emma168-backup/emmasvn_redmine-backup/redmine/ /srv/docker/redmine/redmine/data/tmp/
7z x -y ${EMMA_READMINE_PLUGINSFILE} -o${EMMA_REDMINE_TEMP}/tmp/redmine0/plugins
#rsync -av --ignore-existing ${DIR168_EMMASVN_BACKUP}/emma168-backup/emmasvn_redmine-backup/plugins /srv/docker/redmine/redmine/data/tmp
#rm -rf ${DIR168_EMMASVN_BACKUP}/emma168-backup/emmasvn_redmine-backup/

docker exec -i aalm_redmine_1 bash << EOF
#rm -rf /home/redmine/redmine/files/*
#rm -rf /home/redmine/redmine/plugins/*

cd /home/redmine/redmine
rsync -av --stats --human-readable --ignore-existing  /home/redmine/data/tmp/redmine0/files/* /home/redmine/redmine/files/
rsync -av --stats --human-readable --ignore-existing  /home/redmine/data/tmp/redmine0/plugins/* /home/redmine/redmine/plugins/

echo "###### Redmine plugins #######"
bundle install
bundle exec rake redmine:plugins:migrate RAILS_ENV="production"
chown -R redmine:redmine /home/redmine/redmine/plugins/
EOF

rm -rf ${EMMA_REDMINE_TEMP}/tmp/redmine0

fi

echo "###### Sync Done!! ######"
