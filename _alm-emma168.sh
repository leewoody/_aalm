#!/bin/bash
#
DIR168_HOME=/home/emmauser
cd ${DIR168_HOME}

DIR168_GITTEMP=${DIR168_HOME}/gittemp
EMMA_GIT=${DIR168_HOME}/emma168-backup/emmasvn_git-backup
#EMMA_READMINE=

EMMA_SQLFILE=emmasvn_redmine1120.sql

echo "###### Sync Emma Git #######"

mkdir -p ${DIR168_GITTEMP}
mv ${DIR168_HOME}/emma168-backup/emmasvn_backup/*GIT-Emma.7z ${EMMA_GIT}/
7z x -y ${EMMA_GIT}/*GIT-Emma.7z -o${DIR168_GITTEMP}
rsync -av --ignore-existing ${DIR168_GITTEMP}/ /srv/docker/gitlab/gitlab/repositories/Emma/
rm -rf ${DIR168_GITTEMP}
#rm -rf ${EMMA_GIT}/*GIT-Emma.7z

docker exec -i aalm_gitlab_1 bash << EOF
chown -R git:git /home/git/data/repositories/
cd /home/git/gitlab
bundle exec rake gitlab:import:repos RAILS_ENV=production
EOF

if [[ -n ${USE_EMMASQL} ]]; then
echo "###### Sync Mysql Emmasvn redmine db #######"

#mkdir -p /opt/mysql/emmasvn-redmine-db-temp
rsync -av --ignore-existing ${DIR168_HOME}/emma168-backup/emmasvn_backup/emmasvn_redmine-backup/db/${EMMA_SQLFILE} /srv/docker/mysql/data/
#rm -rf ${DIR168_HOME}/emma168-backup/emmasvn_backup/emmasvn_redmine-backup/db/${EMMA_SQLFILE}

docker exec -i aalm_mysql_1 bash << EOF
cd /var/lib/mysql
mysql -uroot --default-character-set=utf8 < ${EMMA_SQLFILE}
mysql -uroot  << EOF
use redmine;
show tables;
UPDATE users SET hashed_password='353e8061f2befecb6818ba0c034c632fb0bcae1b' WHERE login='admin';
UPDATE users SET salt='' WHERE login='admin';
EOF

EOF

rm -rf  /opt/mysql/data/*.sql
fi

echo "###### Sync Emma Redmine #######"

7z x -y ${DIR168_HOME}/emma168-backup/emmasvn_backup/*www-redmine.7z -o${DIR168_HOME}/emma168-backup/emmasvn_redmine-backup/redmine
rsync -av --ignore-existing ${DIR168_HOME}/emma168-backup/emmasvn_redmine-backup/redmine/files /srv/docker/redmine/redmine/data/files
7z x -y ${DIR168_HOME}/emma168-backup/emmasvn_redmine-backup/plugins/redmine_plugins.7z -o/srv/docker/redmine/redmine/data/tmp/plugins
#rsync -av --ignore-existing ${DIR168_HOME}/emma168-backup/emmasvn_redmine-backup/plugins /srv/docker/redmine/redmine/data/tmp
#rm -rf ${DIR168_HOME}/emma168-backup/emmasvn_redmine-backup/

docker exec -i aalm_redmine_1 bash << EOF
cd /home/redmine/redmine
cp /home/redmine/data/tmp/plugins/* /home/redmine/redmine/plugins/
echo "###### Redmine plugins#######"
bundle install
bundle exec rake redmine:plugins:migrate RAILS_ENV="production"
EOF
rm -rf /srv/docker/redmine/redmine/data/tmp/plugins

echo "Sync Done!!"
