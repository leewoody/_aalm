#!/bin/bash
set -e

mysql -uredmine -ppassword < emmasvn_redmine1120.sql
mysql -uredmine -ppassword << EOF
use redmine;
show tables;
UPDATE users SET hashed_password='353e8061f2befecb6818ba0c034c632fb0bcae1b' WHERE login='admin';
UPDATE users SET salt='' WHERE login='admin';
EOF
