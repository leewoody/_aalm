#!/bin/bash

# This script starts the database server.
dbfile="/db-backup/emmasvn_redmine1120.sql"

echo "Adding data into MySQL"
/usr/sbin/mysqld &
sleep 5

mysqldump -h 127.0.0.1 -u root -ppass --single-transaction --databases redmine > redmine-dbfile.sql

mysql --default-character-set=utf8 < $dbfile

rm $dbfile

mysqladmin shutdown
echo "finished"

# Now the provided user credentials are added
/usr/sbin/mysqld &
sleep 5
echo "Creating user"
echo "CREATE USER 'redmine' IDENTIFIED BY 'redmine'" | mysql --default-character-set=utf8
echo "GRANT ALL PRIVILEGES ON  redmine. * TO '$user'@'%' WITH MAX_USER_CONNECTIONS $max_connections; FLUSH PRIVILEGES" | mysql --default-character-set=utf8
echo "finished"

# And we restart the server to go operational
mysqladmin shutdown
echo "Starting MySQL Server"
/usr/sbin/mysqld
