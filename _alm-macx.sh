#!/bin/bash
#
## scp woodylee@emmasvn.cmes.com.tw:/home/woodylee/backup/backup-emmasvn_2015/20151225233001-GIT-Emma.7z ./
# sync -az --numeric-ids --stats --human-readable --delete  woodylee@emmasvn.cmes.com.tw:/home/woodylee/backup/emmasvn-config ./
#@ https://www.namhuy.net/2433/ssh-login-without-password.html
# rsync -az -e "ssh" --numeric-ids --stats --human-readable --delete /home/woodylee/backup/backup-emmasvn  emmauser@emma168.cmes.com.tw:/home/emmauser/emma168-rsync-backup
# echo $(ls -1t *.rar | head -1)
# 30 23 * * 5 /home/woodylee/backup/backup-emmasvn.sh

set -x

reldir=`dirname $0`
cd $reldir
directory=`pwd`

echo "Directory is $directory"

#DIR168_EMMASVN_BACKUP=/home/emmauser/emma168-rsync-backup/backup-emmasvn
DIR168_EMMASVN_BACKUP=$directory/backup-emmasvn
cd ${DIR168_EMMASVN_BACKUP}

#DIR168_GITTEMP=${DIR168_EMMASVN_BACKUP}/../gittemp

#EMMA_GIT=${DIR168_EMMASVN_BACKUP}/emma168-backup/emmasvn_git-backup
EMMA_GITFILE=$(ls -1t *GIT-*.7z | head -1) #GIT-Emma.7z
EMMA_READMINEFILE=$(ls -1t *www-redmine*.7z | head -1) #www-redmine.7z
EMMA_READMINE_PLUGINSFILE=$(ls -1t *plugins*.tar.gz | head -1) #../redmine_plugins.7z
EMMA_SQLFILE=$(ls -1t *sqlsdump-redmine*.sql.gz | head -1) #mysqldump.sql.gz
EMMA_REDMINE_TEMP=/opt/redmine/data

#USE_EMMAGIT=1
USE_EMMAREDMINE=1
USE_EMMAREDMINE_PLUGINS=1

set +x

if [[ -n ${USE_EMMAREDMINE} ]]; then

echo "###### Sync Mysql Emmasvn redmine db #######"
sudo cp -p ${EMMA_SQLFILE} /opt/mysql/data/

docker exec -i aalm_mysql_1 bash <<'EOF'
cd /var/lib/mysql
gunzip < ${EMMA_SQLFILE} | mysql -uroot --default-character-set=utf8
mysql -uroot  <<'EOF'
use redmine;
show tables;
UPDATE users SET hashed_password='353e8061f2befecb6818ba0c034c632fb0bcae1b' WHERE login='admin';
UPDATE users SET salt='' WHERE login='admin';
EOF
EOF

sudo rm -rf  /opt/mysql/data/${EMMA_SQLFILE}
fi

if [[ -n ${USE_EMMAREDMINE_PLUGINS} ]]; then

echo "###### Sync Emma Redmine #######"
mkdir -p ${EMMA_REDMINE_TEMP}/tmp/redmine0

tar zxf ${EMMA_READMINEFILE} -C ${EMMA_REDMINE_TEMP}/tmp/redmine0
tar zxf ${EMMA_READMINE_PLUGINSFILE} -C ${EMMA_REDMINE_TEMP}/tmp/redmine0

docker exec -i aalm_redmine_1 bash <<'EOF'

cd /home/redmine/redmine
cp -rp  /home/redmine/data/tmp/redmine0/files/* /home/redmine/redmine/files/
cp -rp  /home/redmine/data/tmp/redmine0/plugins/* /home/redmine/redmine/plugins/

echo "###### Redmine plugins #######"
#bundle update
bundle install
bundle exec rake redmine:plugins:migrate RAILS_ENV="production"
chown -R redmine:redmine /home/redmine/redmine/plugins/
EOF

sudo rm -rf ${EMMA_REDMINE_TEMP}/tmp/redmine0

fi

echo "###### Sync Done!! ######"
